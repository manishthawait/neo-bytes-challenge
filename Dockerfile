FROM alpine

RUN mkdir -p /home/src

WORKDIR /home/src

COPY my_first_script.sh /home/src

RUN chmod +x /home/src/my_first_script.sh

ENTRYPOINT ["/bin/sh", "/home/src/my_first_script.sh"]
